package fr.winside.bank.repository;

import fr.winside.bank.model.Client;
import fr.winside.bank.model.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<Operation, String> {

    List<Operation> findOperationsByAccount_Client(Client client);
}
