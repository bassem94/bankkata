package fr.winside.bank.service;

import fr.winside.bank.model.Account;
import fr.winside.bank.model.Client;
import fr.winside.bank.model.Operation;
import fr.winside.bank.repository.AccountRepository;
import fr.winside.bank.repository.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OperationService implements IOperationService {

    private OperationRepository operationRepository;
    private final AccountRepository accountRepository;

    public OperationService(OperationRepository operationRepository, AccountRepository accountRepository) {
        this.operationRepository = operationRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Operation addOperation(Operation operation) {
        Operation savedOperation = operationRepository.save(operation);
        Account account = savedOperation.getAccount();
        account.setAmount(account.getAmount() + operation.getAmount());
        accountRepository.save(account);
        return savedOperation;
    }

    @Override
    public List<Operation> getOperationsList(Client client) {
        return operationRepository.findOperationsByAccount_Client(client);
    }
}
