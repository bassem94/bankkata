package fr.winside.bank.service;

import fr.winside.bank.model.Account;
import fr.winside.bank.model.Client;

public interface IAccountService {

    public Account createAccount(Account account);
    public Account getAccountByClient(Client client);
}
