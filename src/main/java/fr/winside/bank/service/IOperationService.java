package fr.winside.bank.service;

import fr.winside.bank.model.Account;
import fr.winside.bank.model.Client;
import fr.winside.bank.model.Operation;

import java.util.List;

public interface IOperationService {
    public Operation addOperation(Operation operation);
    List<Operation> getOperationsList(Client client);
}
