package fr.winside.bank.service;

import fr.winside.bank.model.Client;
import fr.winside.bank.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.util.Objects.requireNonNull;

@Component
public class ClientService implements IClientService {

    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = requireNonNull(clientRepository);
    }


    @Override
    public Client createClient(Client client) {
        return clientRepository.save(client);
    }




}
