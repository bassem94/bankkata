package fr.winside.bank.service;

import fr.winside.bank.model.Account;
import fr.winside.bank.model.Client;

public interface IClientService {
    Client createClient(Client client);
}
