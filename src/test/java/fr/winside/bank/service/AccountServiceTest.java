package fr.winside.bank.service;

import fr.winside.bank.model.Account;
import fr.winside.bank.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@SpringBootTest
public class AccountServiceTest {
    private static final String ACCOUNT_ID = UUID.randomUUID().toString();

    @Mock
    private AccountRepository mockAccountRepository;
    @Mock
    private AccountService accountService;


    @BeforeEach
    void before() {
        this.accountService = new AccountService(mockAccountRepository);
    }

    @Test
    void testCreateAccount() {
        when(mockAccountRepository.save(any())).thenReturn(expectedAccountObject());
        Account result = accountService.createAccount(new Account());
        assertThat(result).isEqualTo(expectedAccountObject());

    }


    @Test
    void testGetAccountByClient() {
        when(mockAccountRepository.findAccountByClientId(any())).thenReturn(Optional.of(expectedAccountObject()));

        // Run the test
        final Account result = accountService.getAccountByClient(ClientServiceTest.expectedClientObject());

        // Verify the results
        assertThat(result).isEqualTo(expectedAccountObject());
    }

    static Account expectedAccountObject() {
        return Account.builder().id(ACCOUNT_ID).name("name").client(ClientServiceTest.expectedClientObject()).amount(5000).build();
    }

}
