package fr.winside.bank.service;

import fr.winside.bank.model.Client;
import fr.winside.bank.repository.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    private static final String CLIENT_ID = UUID.randomUUID().toString();
    @Mock
    private ClientRepository mockClientRepository;


    private ClientService clientService;

    @BeforeEach
    void setUp() {
        clientService = new ClientService(mockClientRepository);
    }


    @Test
    void testCreateClient(){
        when(mockClientRepository.save(any())).thenReturn(expectedClientObject());
        Client client = clientService.createClient(new Client());
        assertThat(client).isEqualTo(expectedClientObject());
    }

    static Client expectedClientObject(){
        return Client.builder().id(CLIENT_ID).firstname("Bassem").lastname("ZOUARI").build();
    }
}
