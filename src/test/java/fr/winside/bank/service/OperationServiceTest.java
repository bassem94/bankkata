package fr.winside.bank.service;

import fr.winside.bank.model.Account;
import fr.winside.bank.model.Client;
import fr.winside.bank.model.Operation;
import fr.winside.bank.repository.AccountRepository;
import fr.winside.bank.repository.OperationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OperationServiceTest {
    private static final String OPERATION_ID = UUID.randomUUID().toString();

    @Mock
    private OperationRepository mockOperationRepository;
    @Mock
    private AccountRepository mockAccountRepository;

    private OperationService operationService;

    @BeforeEach
    void setUp() {
        operationService = new OperationService(mockOperationRepository, mockAccountRepository);
    }

    @Test
    void testAddOperation() {
        // Setup
        when(mockOperationRepository.save(any())).thenReturn(expectedResultOperation());

        // Configure AccountRepository.save(...).
        when(mockAccountRepository.save(any())).thenReturn(expectedResultOperation().getAccount());

        // Run the test
        final Operation result = operationService.addOperation(Operation.builder().amount(300).build());

        // Verify the results
        assertThat(result.getAccount().getAmount()).isEqualTo(expectedResultOperation().getAccount().getAmount() + expectedResultOperation().getAmount());
        assertThat(result.getAccount().getAmount()).isNotEqualTo(expectedResultOperation().getAccount().getAmount());
    }

    @Test
    void testWithdrawalMoney() {

        double retrait = 1100;
        when(mockOperationRepository.save(any())).thenReturn(expectedResultOperation());

        // Configure AccountRepository.save(...).
        when(mockAccountRepository.save(any())).thenReturn(expectedResultOperation().getAccount());

        // Run the test
        final Operation result = operationService.addOperation(Operation.builder().amount(-retrait).build());
        System.out.println(result.getAccount().getAmount());
        System.out.println(expectedResultOperation().getAccount().getAmount());
        System.out.println(expectedResultOperation().getAmount());
        // Verify the results
        assertThat(result.getAccount().getAmount()).isNotEqualTo(expectedResultOperation().getAccount().getAmount());
        assertThat(result.getAccount().getAmount()).isEqualTo(expectedResultOperation().getAccount().getAmount() - retrait);
    }


    @Test
    void testGetMyOperationsList() {
        // Setup

        when(mockOperationRepository.findOperationsByAccount_Client(ClientServiceTest.expectedClientObject())).thenReturn(myOperations());

        // Run the test
        final List<Operation> result = operationService.getOperationsList(ClientServiceTest.expectedClientObject());

        // Verify the results
        assertThat(result.size()).isEqualTo((int) allOperations().stream()
                .filter(operation -> Objects.equals(operation.getAccount().getClient().getId(), ClientServiceTest.expectedClientObject().getId())).count());
    }

    Operation expectedResultOperation() {
        return Operation.builder().id(OPERATION_ID).account(AccountServiceTest.expectedAccountObject()).amount(300).build();
    }

    List<Operation> allOperations() {
        List<Operation> operations = new ArrayList<>(myOperations());
        operations.add(Operation.builder().id("1").amount(200).account(Account.builder().client(new Client()).build()).build());
        return operations;
    }

    List<Operation> myOperations() {
        List<Operation> operations = new ArrayList<>();
        operations.add(Operation.builder().id("1").amount(400).account(AccountServiceTest.expectedAccountObject()).build());
        operations.add(Operation.builder().id("1").amount(-1000).account(AccountServiceTest.expectedAccountObject()).build());
        operations.add(Operation.builder().id("1").amount(500).account(AccountServiceTest.expectedAccountObject()).build());
        return operations;
    }

}
